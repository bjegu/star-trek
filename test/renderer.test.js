
import Renderer from "../src/utils/renderer.js";
import { EOL } from "os";

class Point {
    constructor(x, y){
        this.x = x;
        this.y = y;
    }
}

class GalaxyObject {
    constructor(quadrant, sector, type){
        this.type = type;
        this.setPosition(quadrant, sector);
        this.destroy = false;
    }

    setPosition(quadrant, sector){
        this.quadrant = quadrant;
        this.sector = sector;
    }
}

class Ship extends GalaxyObject {
    constructor(quadrant, sector, power){
        super(quadrant, sector, 'ship');
        this.power = power;
    }
}

const quadrantPoint = new Point(3, 3);
const player =  new Ship(quadrantPoint, new Point(3, 3), 600);

describe("Test renderEmptyQuadrant", () => {
    it("Should display quadrant of given schema", () => {
        const expected =
            `  _0__1__2__3__4__5__6__7_ ${EOL}` +
            `7| .  .  .  .  .  .  .  . |${EOL}` +
            `6| .  .  .  .  .  .  .  . |${EOL}` +
            `5| .  .  .  .  .  .  .  . |${EOL}` +
            `4| .  .  .  .  .  .  .  . |${EOL}` +
            `3| .  .  .  .  .  .  .  . |${EOL}` +
            `2| .  .  .  .  .  .  .  . |${EOL}` +
            `1| .  .  .  .  .  .  .  . |${EOL}` +
            `0| .  .  .  .  .  .  .  . |${EOL}`;
        let result = Renderer.renderEmptyQuadrant();
        expect(result).toMatch(expected);
    });
});

describe("Test renderQuadrant", () => {
    it("Should display player", () => {
        const expected =
            `  _0__1__2__3__4__5__6__7_ ${EOL}` +
            `7| .  .  .  .  .  .  .  . |${EOL}` +
            `6| .  .  .  .  .  .  .  . |${EOL}` +
            `5| .  .  .  .  .  .  .  . |${EOL}` +
            `4| .  .  .  .  .  .  .  . |${EOL}` +
            `3| .  .  . <*> .  .  .  . |${EOL}` +
            `2| .  .  .  .  .  .  .  . |${EOL}` +
            `1| .  .  .  .  .  .  .  . |${EOL}` +
            `0| .  .  .  .  .  .  .  . |${EOL}`;
        let result = Renderer.renderQuadrant(player, []);
        expect(result).toMatch(expected);
    });
    it("Should display all objects in quadrant", () => {
        const objectsInQuadrant = [
            new Ship(quadrantPoint, new Point(0, 0), 324),
            new Ship(quadrantPoint, new Point(5, 6), 123),
            new Ship(quadrantPoint, new Point(7, 7), 444),
            new Ship(quadrantPoint, new Point(7, 0), 555),
            new Ship(quadrantPoint, new Point(4, 3), 321),
            new GalaxyObject(quadrantPoint, new Point(1, 7), 'star'),
            new GalaxyObject(quadrantPoint, new Point(3, 5), 'star'),
            new GalaxyObject(quadrantPoint, new Point(3, 0), 'base'),
            new GalaxyObject(quadrantPoint, new Point(1, 5), 'base')
        ]
        const expected =
            `  _0__1__2__3__4__5__6__7_ ${EOL}` +
            `7| .  *  .  .  .  .  . +++|${EOL}` +
            `6| .  .  .  .  . +++ .  . |${EOL}` +
            `5| . >!< .  *  .  .  .  . |${EOL}` +
            `4| .  .  .  .  .  .  .  . |${EOL}` +
            `3| .  .  . <*>+++ .  .  . |${EOL}` +
            `2| .  .  .  .  .  .  .  . |${EOL}` +
            `1| .  .  .  .  .  .  .  . |${EOL}` +
            `0|+++ .  . >!< .  .  . +++|${EOL}`;
        let result = Renderer.renderQuadrant(player, objectsInQuadrant);
        expect(result).toMatch(expected);
    });
});