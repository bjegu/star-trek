import StarTrek from './../src/core/starTrek'
import Ship from './../src/core/ship'
import Point from './../src/utils/point'

let starTrek = new StarTrek()

describe("Test construtor", () => {
    it('Should generate player', () => {
        expect(starTrek.player).toBeInstanceOf(Ship);
        expect(starTrek.player.power).toBe(600);
        expect(starTrek.player.quadrant).toEqual(new Point(3, 3));
        expect(starTrek.player.sector).toEqual(new Point(3, 3));
    });
});

describe("Test movePlayer", () => {
    beforeEach(() => {
        starTrek.player.setPosition(new Point(1, 1), new Point(1, 1));
        starTrek.player.power = 600;
    });

    it("Should move player", () => {
        starTrek.movePlayer(6, 6);
        const expected = {
            quadrant: {
                x: 1,
                y: 1
            },
            sector: {
                x: 7,
                y: 7
            }
        };
        expect(starTrek.player.getPosition()).toEqual(expected);
    });

    it("Should move player", () => {
        starTrek.movePlayer(7, 7);
        const expected = {
            quadrant: {
                x: 2,
                y: 2
            },
            sector: {
                x: 0,
                y: 0
            }
        };
        expect(starTrek.player.getPosition()).toEqual(expected);
    });

    it("Should move player", () => {
        starTrek.movePlayer(6, 7);
        const expected = {
            quadrant: {
                x: 1,
                y: 2
            },
            sector: {
                x: 7,
                y: 0
            }
        };
        expect(starTrek.player.getPosition()).toEqual(expected);
    });

    it("Should move player", () => {
        starTrek.movePlayer(-2, -2);
        const expected = {
            quadrant: {
                x: 0,
                y: 0
            },
            sector: {
                x: 7,
                y: 7
            }
        };
        expect(starTrek.player.getPosition()).toEqual(expected);
    });

    it("Should not move player", () => {
        starTrek.movePlayer(0, 0);
        const expected = {
            quadrant: {
                x: 1,
                y: 1
            },
            sector: {
                x: 1,
                y: 1
            }
        };

        expect(starTrek.player.getPosition()).toEqual(expected);
    });

    it("Should not move player because out of galaxy boundaries", () => {
        starTrek.movePlayer(-37, 0);
        let expected = {
            quadrant: {
                x: 1,
                y: 1
            },
            sector: {
                x: 1,
                y: 1
            }
        };

        expect(starTrek.player.getPosition()).toEqual(expected);

        starTrek.movePlayer(0, -37);
        expected = {
            quadrant: {
                x: 1,
                y: 1
            },
            sector: {
                x: 1,
                y: 1
            }
        };

        expect(starTrek.player.getPosition()).toEqual(expected);

        starTrek.movePlayer(55, 0);
        expected = {
            quadrant: {
                x: 1,
                y: 1
            },
            sector: {
                x: 1,
                y: 1
            }
        };

        expect(starTrek.player.getPosition()).toEqual(expected);

        starTrek.movePlayer(0, 55);
        expected = {
            quadrant: {
                x: 1,
                y: 1
            },
            sector: {
                x: 1,
                y: 1
            }
        };

        expect(starTrek.player.getPosition()).toEqual(expected);
    });


});