import Galaxy from '../src/core/galaxy.js';
import GalaxyObject from '../src/core/galaxyObject.js';

const galaxyConfig = {
    QUADRANTS: 64,
    SECTORS_IN_QUADRANT: 64,
    KLINGON_SHIPS: 7,
    STARBASES: 2,
    STARS: 20
}

let newgalaxy = new Galaxy(galaxyConfig)

describe("Test generateGameObjects", () => {
    it("Should erturn an array of SpaceObjects", () => {
        const result = newgalaxy.generateGameObjects();
        expect(result).toBeInstanceOf(Array);
        expect(result.length).toBe(29);
        result.forEach(el => expect(el).toBeInstanceOf(GalaxyObject));
    });
    it("Should return proper objects", () => {
        const result = newgalaxy.generateGameObjects();
        
        const ships = result.filter(el => el.type == 'ship')
        const starbase = result.filter(el => el.type == 'starbase')
        const stars = result.filter(el => el.type == 'star')

        expect(ships.length).toBe(galaxyConfig.KLINGON_SHIPS);
        expect(starbase.length).toBe(galaxyConfig.STARBASES);
        expect(stars.length).toBe(galaxyConfig.STARS);
    });
});