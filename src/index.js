// the game start

import StarTrek from "./core/starTrek";
import { readRestartDecision, readCommand, MANOEUVRE_COMMAND, BATTLE_COMMAND, SHORT_SCAN_COMMAND, LONG_SCAN_COMMAND, GALAXY_MAP_COMMAND, readVector, readAmountOfEnergy } from "./utils/inputReader.js";
import Renderer from './utils/renderer'

const COMMANDS_MODE = 0,
MANOEUVRE_MODE = 1,
BATTLE_MODE = 2

let game = new StarTrek()
let mode = COMMANDS_MODE


while(!game.over) {
    console.log(Renderer.playerStatus(game.player));
    if(mode === COMMANDS_MODE){
        let command = readCommand();
        switch (command) {
            case MANOEUVRE_COMMAND:
                console.log("ENTERING INTO THE MANOEUVRE MODE");
                mode = MANOEUVRE_MODE;
                break;
            case BATTLE_COMMAND:
                console.log("ENTERING INTO THE BATTLE MODE");
                mode = BATTLE_MODE;
                break;
            case SHORT_SCAN_COMMAND:
                console.log(Renderer.renderQuadrant(game.player, game.galaxy.getQuadrantObjects(game.player.quadrant)));
                break;
            case LONG_SCAN_COMMAND:
                break;
            case GALAXY_MAP_COMMAND:
                break;
        }
    } else if(mode === MANOEUVRE_MODE) {
        let vector = readVector();
        mode = COMMANDS_MODE;
        game.movePlayer(vector[0], vector[1])
    } else if( mode === BATTLE_MODE) {
        console.log(`ENTERPRISE HAS ${game.player.power} ENERGY`);
        let amountOfEnergy = readAmountOfEnergy();
        if (amountOfEnergy === 0) {
            console.log("ENTERING INTO THE COMMANDS MODE");
            mode = COMMANDS_MODE;
        } else {
            game.shoot(amountOfEnergy)
        }
    } 
    game.endingScenario()
    if (game.over) {
        let answer = "";
        while (answer != "y" && answer != "n") {
            console.log(answer);
            answer = readRestartDecision();
        }
        if (answer == "y") game = new StarTrek();
    }
}