import { EOL } from "os";

const gameSymbols = {
    player: "<*>",
    ship: "+++",
    base: ">!<",
    star: " * "
};

const quadrantSize = 8

function renderQuadrant(player, gameObjects) {
    let render = renderEmptyQuadrant();
    // Render Player
    render = drawSymbol(
        render, 
        gameSymbols.player, 
        convertPositionToString(player.sector.x, player.sector.y))
    // Render Other Objects
    for (let i = 0; i < gameObjects.length; i++) {
        const symbol = gameSymbols[gameObjects[i].type]
        const objectPos = convertPositionToString(gameObjects[i].sector.x, gameObjects[i].sector.y);
        render = drawSymbol(
            render, 
            symbol, 
            objectPos)
    } 
    return render;
}

function drawSymbol(render, symbol, position) {
    return render.substr(0, position) + symbol + render.substr(position + 3);
}

function convertPositionToString(x, y) {
    let rowLength = 3 * quadrantSize + 3 + EOL.length;
    return rowLength * quadrantSize - y * rowLength + 2 + x * 3;
}

function renderEmptyQuadrant() {
    let quadrantBoundaries = "  ";
    for (let i = 0; i < quadrantSize; i++) {
        quadrantBoundaries += `_${i}_`;
    }
    quadrantBoundaries += ` ${EOL}`;
    for (let x = 0; x < quadrantSize; x++) {
        quadrantBoundaries += quadrantSize - 1 - x + "|";
        for (let y = 0; y < quadrantSize; y++) {
            quadrantBoundaries += " . ";
        }
        quadrantBoundaries += "|" + EOL;
    }
    return quadrantBoundaries;
}

function playerStatus(player) {
    return `${EOL}Quadrant: ${player.quadrant.x}|${player.quadrant.y},${EOL}Sector: ${player.sector.x}|${player.sector.y},${EOL}Power: ${player.power}${EOL}`;
}

export default {
    renderQuadrant,
    renderEmptyQuadrant,
    playerStatus
};
