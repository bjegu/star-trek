export function distance(x1, y1, x2, y2){
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}

export function inRange(value, min, max){
    if (min > max)
        throw new RangeError('Min value cannot be biger than max');
    return value >= min && value <= max;
}

export function hasSamePos(pos1, pos2){
    return (pos1.x == pos2.x && pos1.y == pos2.y);
}

export default {
    distance,
    hasSamePos,
    inRange
};
