import { question } from 'readline-sync';

export const MANOEUVRE_COMMAND = 0;
export const SHORT_SCAN_COMMAND = 1;
export const LONG_SCAN_COMMAND = 2;
export const BATTLE_COMMAND = 3;
export const GALAXY_MAP_COMMAND = 4;
export function readCommand() {
    return parseInt(question('Command: ', {
        limit: [
            MANOEUVRE_COMMAND,
            SHORT_SCAN_COMMAND,
            LONG_SCAN_COMMAND,
            BATTLE_COMMAND,
            GALAXY_MAP_COMMAND
        ],
        limitMessage: 'INCORRECT COMMAND, PLEASE USE ONE OF THESE COMMANDS: $<limit>'
    }));
}
export function readVector() {
    return question('VECTOR ? ', {
        limit: [/^-?\d+,-?\d+$/],
        limitMessage: 'INCORRECT VECTOR FORMAT, PLEASE USE THE FOLLOWING FORMAT: X,Y'
    }).split(',').map(x => parseInt(x));
}
export function readAmountOfEnergy() {
    return parseInt(question('How much energy use to attack? ', {
        limit: [/\d+/],
        limitMessage: 'INCORRECT AMOUNT OF ENERGY'
    }));
}
export function readRestartDecision() {
    return question('Do you want to start again? (y/n): ', {
        limit: [/y|n/],
        limitMessage: 'INCORRECT ANSWER Y/N'
    });
}