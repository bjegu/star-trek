import Ship from "./ship";
import Point from "./../utils/point"
import Galaxy from './galaxy'
import { distance, inRange, hasSamePos } from "../utils/mathUtils";

export default class StarTrek {

    constructor() {
        this.config = {
            GALAXY : {
                QUADRANTS: 64,
                SECTORS_IN_QUADRANT: 64,
                KLINGON_SHIPS: 7,
                STARBASES: 2,
                STARS: 20
            },
            MAX_POWER : 600,
            INITIAL_STARDATES : 30
        }
        this.over = 0
        this.player = new Ship(new Point(3, 3), new Point(3, 3), this.config.MAX_POWER);
        this.starDates = this.config.INITIAL_STARDATES;
        this.galaxy = new Galaxy(this.config.GALAXY);
    }

    shoot(power) {
        if(power> this.player.power){
            console.log('Not enough power');
            return;
        }
        let target = this.galaxy.getQuadrantObjects(this.player.quadrant)
            .filter(so => so.type == 'ship')
            .sort((prev, cur) => {
                let prevDist = distance(this.player.sector.x, this.player.sector.y, prev.sector.x, prev.sector.y);
                let curDist = distance(this.player.sector.x, this.player.sector.y, cur.sector.x, cur.sector.y);
                return prevDist > curDist;
            })[0];
        if(target == null){
            console.log('There is no remaining klingons in your current quadrant');
        } else {
            this.player.reducePower(power);
            let distanceToPlayer = distance(
                this.player.sector.x,
                this.player.sector.y,
                target.sector.x,
                target.sector.y
            );
            let hasHit = Math.random() < (5 / (distanceToPlayer + 4));
            if (hasHit){
                target.reducePower(power);
                if(target.power <= 0){
                    target.destroy = true;
                    console.log('You have destroyed klingon!');
                } else {
                    console.log(`You have hit klingon, enemy has ${target.power} more power.`);
                }
            } else {
                console.log('You missed!');
            }
        }
    }

    movePlayer(xOffset, yOffset) {
        const previousSectorNumber = this.galaxy.getSectorNumberFromPosition(this.player.quadrant, this.player.sector);
        const previousGlobalPos = this.galaxy.getGlobalPositionFromSectorNumber(previousSectorNumber);

        const newSectorNumber = previousSectorNumber + xOffset + yOffset * 64;

        const newQuadrant = this.galaxy.getQuadrantFromSectorNumber(newSectorNumber);
        const newSector = this.galaxy.getSectorFromSectorNumber(newSectorNumber);

        const currentGlobalPos = this.galaxy.getGlobalPositionFromSectorNumber(newSectorNumber);

        const quadrantDistance = distance(
            this.player.quadrant.x,
            this.player.quadrant.y,
            newQuadrant.x,
            newQuadrant.y
        );
        const distanceTraveled = distance(
            previousGlobalPos.x,
            previousGlobalPos.y,
            currentGlobalPos.x,
            currentGlobalPos.y
        );
        if (
            inRange(previousGlobalPos.x + xOffset, 0, 63) &&
            inRange(previousGlobalPos.y + yOffset, 0, 63) &&
            this.starDates - quadrantDistance >= 0 &&
            this.player.power - distanceTraveled > 0
        ) {
            if (!hasSamePos(this.player.quadrant, newQuadrant))
                this.galaxy.shuffleQuadrant(this.player.quadrant);

            this.starDates -= quadrantDistance;

            this.player.setPosition(newQuadrant, newSector);
            this.player.reducePower(distanceTraveled);
            this.detectPlayerCollisions();
        } else if (this.starDates - quadrantDistance < 0) {
            console.log("You do not have enough time");
        } else if (this.player.power - distanceTraveled <= 0) {
            console.log("You do not have enough power");
        } else {
            console.log("You cannot leave galaxy!");
        }
    }

    detectPlayerCollisions() {
        this.galaxy.getQuadrantObjects(this.player.quadrant).forEach(so => {
            if (hasSamePos(so.sector, this.player.sector)) {
                this.player.destroy = true;
                return;
            } else if (so.type == "starbase") {
                this.galaxy.getSurrouningSectors(so.sector).forEach(p => {
                    if (hasSamePos(p, this.player.sector)) this.player.power = this.gameConfig.MAX_POWER;
                });
            }
        });
    }

    endingScenario(){
        if (this.player.destroy) {
            this.over = true;
            console.log("Enterprise has been destroyed!");
        }
    }
}