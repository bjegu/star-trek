import Point from './../utils/point'
import GalaxyObject from './galaxyObject'
import Ship from './ship'
import MathUtils from './../utils/mathUtils'

export default class Galaxy {
    constructor(galaxyConfig) {
        this.config = galaxyConfig;
        this.SpaceObjects = this.generateGameObjects();
    }

    generateGameObjects() {
        let gameObjects = []
        const sectInGalaxy = this.config.QUADRANTS * this.config.SECTORS_IN_QUADRANT;
        const max = sectInGalaxy - 1
        const min = 0

        for(let i = 0; i < this.config.KLINGON_SHIPS; i++){
            const random =  Math.floor(Math.random() * (max - min)) + min
            const quadrant = this.getQuadrantFromSectorNumber(random);
            const sector = this.getSectorFromSectorNumber(random);
            gameObjects.push(new Ship(quadrant, sector, 200));
        }

        for(let i = 0; i < this.config.STARS; i++){
            const random =  Math.floor(Math.random() * (max - min)) + min
            const quadrant = this.getQuadrantFromSectorNumber(random);
            const sector = this.getSectorFromSectorNumber(random);
            gameObjects.push(new GalaxyObject(quadrant, sector, "star"));
        }

        for(let i = 0; i < this.config.STARBASES; i++){
            const random =  Math.floor(Math.random() * (max - min)) + min
            const quadrant = this.getQuadrantFromSectorNumber(random);
            const sector = this.getSectorFromSectorNumber(random);
            gameObjects.push(new GalaxyObject(quadrant, sector, "starbase"));
        }
        return gameObjects;
    }


    getQuadrantFromSectorNumber(sectorNumber) {
        const sectInQuad = this.config.SECTORS_IN_QUADRANT;
        const nofQuad = this.config.QUADRANTS;
        let y = Math.floor(sectorNumber / (Math.sqrt(nofQuad) * sectInQuad));
        let x = Math.floor(sectorNumber / Math.sqrt(nofQuad)) % Math.sqrt(sectInQuad);
        return new Point(x, y);
    }

    getSectorFromSectorNumber(sectorNumber) {
        const sectInQuad = this.config.SECTORS_IN_QUADRANT;
        let y = Math.floor(sectorNumber / sectInQuad) % Math.sqrt(sectInQuad);
        let x = sectorNumber % Math.sqrt(sectInQuad);
        return new Point(x, y);
    }

    getSectorNumberFromPosition(quadrant, sector) {
        const sectInQuad = this.config.SECTORS_IN_QUADRANT;
        const nofQuad = this.config.QUADRANTS;
        return (
            quadrant.x * Math.sqrt(nofQuad) +
            quadrant.y * Math.sqrt(nofQuad) * sectInQuad +
            sector.x +
            sector.y * sectInQuad
        );
    }

    getGlobalPositionFromSectorNumber(sectorNumber) {
        const sectInQuad = this.config.SECTORS_IN_QUADRANT;
        const nofQuad = this.config.QUADRANTS;
        return new Point(sectorNumber % sectInQuad, Math.floor(sectorNumber / nofQuad));
    }

    getQuadrantObjects(quadrant) {
        return this.SpaceObjects.filter(so => MathUtils.hasSamePos(so.quadrant, quadrant));
    }

    shuffleQuadrant(quadrant) {
        this.SpaceObjects = this.SpaceObjects.map(so => {
            if(so.quadrant.x == quadrant.x && so.quadrant.y == quadrant.y){
                const random =  Math.floor(Math.random() * this.config.SECTORS_IN_QUADRANT - 1)
                let newX = random % Math.sqrt(this.config.SECTORS_IN_QUADRANT);
                let newY = Math.floor(random / Math.sqrt(this.config.SECTORS_IN_QUADRANT));
                so.setPosition(so.quadrant, new Point(newX, newY));
            }
            return so;
        });
    }

    getSurrouningSectors(sector){
        return [
            new Point(sector.x - 1, sector.y),
            new Point(sector.x, sector.y - 1),
            new Point(sector.x + 1, sector.y),
            new Point(sector.x, sector.y + 1)
        ]
    }
}