import GalaxyObject from './galaxyObject'
export default class Ship extends GalaxyObject {
    constructor(quadrant, sector, power){
        super(quadrant, sector, 'ship');
        this.power = power;
    }

    reducePower(powNumber) {
        this.power -= powNumber;
        if(this.power <= 0)
            this.destroy = true;
    }
}